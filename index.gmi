```
     __ __       __                 __         __          
 .--|  |__.-----|  |_.----.-----.  |  |_.--.--|  |--.-----.
 |  _  |  |__ --|   _|   _|  _  |__|   _|  |  |  _  |  -__|
 |_____|__|_____|____|__| |_____|__|____|_____|_____|_____|

```
# WELCOME TO DISTRO.TUBE
Distro.tube is a site created by Derek Taylor (DT), creator of the DistroTube (DT) channel on YouTube and LBRY.  I create videos about GNU/Linux, free software, open source software and related interests.  This site is best viewed using the Gemini protocol using a Gemini browser such as Amfora (terminal-based client), Lagrange (GUI client) or using the Geminize plugin for Firefox.

=> https://youtube.com/DistroTube DistroTube on YouTube
=> https://odysee.com/@DistroTube:2 DistroTube on Odysee (LBRY)
=> https://patreon.com/distrotube/ Support DT on Patreon
=> https://gitlab.com/dwt1/ DT's GitLab
=> https://gitlab.com/dwt1/distro.tube Submit Guest Articles On GitLab

## VIDEO LIBRARY
Here are links to my videos hosted on Odysee (LBRY), a free and open source alternative to YouTube.

=> videos/2021-videos.gmi Videos from 2021
=> videos/2020-videos.gmi Videos from 2020
=> videos/2019-videos.gmi Videos from 2019
=> videos/2018-videos.gmi Videos from 2018
=> videos/2017-videos.gmi Videos from 2017

## ARTICLES
Here are some articles written by me that some may find useful.  They are mostly Linux and software-related.

=> articles/social-media-implements-new-policies.gmi Social Media Sites Implement New Policies To Combat Wrong-Think (07-20-2020)
=> articles/fish-is-the-better-shell.gmi Fish Is The Better Shell For More Modern Times (04-09-2020)
=> articles/give-new-linux-users-ubuntu.gmi Give New Linux Users Ubuntu, Not Choice (03-28-2020)
=> articles/standardized-keybindings-tiling.gmi Standardized Keybindings Across All Tiling Window Managers (03-12-2020)
=> articles/why-linux-users-are-elitist.gmi Why Do Most Linux Users Have An Elitist Attitude? (03-07-2020)
=> articles/file-globbing-in-linux.gmi File Globbing In Linux (02-26-2020)
=> articles/securing-the-firefox-browser.gmi Securing The Firefox Web Browser (02-25-2020)
=> articles/move-your-home-folder-to-second-drive.gmi Move Your Home Folder To A Second Drive (02-20-2020)
=> articles/the-foss-code-of-conduct.gmi The FOSS Code Of Conduct (01-20-2020)
=> articles/seven-things-to-avoid-on-linux.gmi Seven Things To Avoid On Linux (01-14-2020)
=> articles/installing-lets-encrypt-on-ubuntu.gmi Installing Let's Encrypt On Your Ubuntu Server (01-04-2020)

## GUEST ARTICLES
These are guest articles submitted by members of the community.  The source of this site and all of its content is availble on my GitLab.  Thus, anyone that wants to submit an article can do so with a pull request.  Please, submit your articles in "gemtext" and not HTML or markdown.

=> guest-articles/interactive-dotfile-management-dotbare.gmi Interactive Dotfile Management With Dotbare (05-25-2020) [Kevin Zhuang]
=> guest-articles/managing-dotfiles-with-rcm.gmi Managing Dotfiles With Style With rcm (04-23-2020) [Ronnie Nissan]
=> guest-articles/installing-manjaro-dual-boot.gmi Installing Manjaro in Dual Booting Environment (04-22-2020) [Primož Ajdišek "Bigpod"]
=> guest-articles/installing-and-using-sxhkd.gmi Installing And Using sxhkd (04-21-2020) [Ronnie Nissan]
=> guest-articles/using-dwmblocks-with-dwms-bar.gmi Using dwmblocks With dwm's Bar (04-20-2020) [Ronnie Nissan]
=> guest-articles/how-to-install-suckless-tools.gmi How to Install, Customize and Backup Suckless Tools (04-18-2020) [Ronnie Nissan]
=> guest-articles/its-time-for-new-text-editor.gmi It's Time For a New Text Editor (03-20-2020) [Klaus-Dieter Schmatz]
=> guest-articles/vim-plugins-without-manager.gmi Vim Plugins Without a Plugin Manager (03-15-2020) [Klaus-Dieter Schmatz]

## PROJECT GEMINI
This site uses a new Internet protocol called Gemini which can be seen as a middle ground between the old gopher protocol of the early 1990s and the modern web.  Gemini attempts to address the weaknesses of gopher while avoiding the pitfalls of the web.

I have built this site (or "capsule") using Gemini in an effort to promote the project in the hope that more people will turn away from the dumpster fire that is the modern web.  Gemini is plain text written in a markdown-like syntax.  Links can only be written and displayed one per line (similar to gopher).  There are no images, no video and no advertisements!

Gemini should be of particular interest to people who are:
* Opposed to the web's ubiquitous user tracking
* Tired of obnoxious adverts, autoplaying videos and other misfeatures
* Interested in low-power computing and/or low-speed networks
* Opposed to the big tech monopoly that dominates the web

If you want to help promote a simpler and more efficient alternative to the web, please consider making your next "web" site a Gemini capsule instead.

=> gemini://gemini.circumlunar.space Project Gemini
=> gemini://geminispace.info Gemini Search Engine

## INTERESTING GEMINI CAPSULES
=> gemini://gem.chriswere.uk Chris Were
=> gemini://gemini.barca.mi.it fnt400
=> gemini://friendo.monster Friendo (uoou)
=> gemini://hexdsl.co.uk Hex DSL
=> gemini://gemini.tunerapp.org Internet Radio Stations Directory
=> gemini://medusae.space medusae.space Gemini directory
